from src.location import getLocation
import pandas as pd
import time

df = pd.read_csv('../../files/seller_info.csv', sep='\t')
seller_data = df.values.tolist()
finalData = []

for i in range(len(df)):
    data = [str(x) for x in seller_data[i]]
    sellerAdd = " ".join(data[3:])
    loc = getLocation.getGeoLocation(sellerAdd)
    print(data[0], loc)
    data.append(loc)
    data = [data[1:]]
    finalDf = pd.DataFrame(data)
    finalDf.to_csv("sellerAdd.csv",
                   index=False,
                   header=False,
                   mode='a')
