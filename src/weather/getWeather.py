from flask import Flask, request
import requests
import json
import haversine as hs

app = Flask(__name__)


def getWeatherDetails(coordinates):
    # Getting weather details for coordinates(latitute and longitute)
    weatherData = dict()
    try:
        coordinates = coordinates.split(';')
        response = requests.get('http://api.openweathermap.org/data/2.5/weather?lat=' + str(coordinates[0]) +
                                '&lon=' + str(coordinates[1]) + '&type=hour&start=1369728000&end=1369789200&appid=04c400b205ce837077ee22aec3d17633')
        weatherData = json.loads(response.content.decode('utf-8'))
        print(weatherData)
    except Exception as e:
        print(e)
        pass
    return weatherData


@app.route('/', methods=["GET", "POST"])
def getWeatherData():
    if request.method == "POST":
        coordinates = request.values['coordinates']
        if coordinates:
            return getWeatherDetails(coordinates)
    return False
