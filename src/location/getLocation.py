from flask import Flask, request
import requests
import json
import haversine as hs


app = Flask(__name__)


def getDistanceBetweenTwoLocation(loc1, loc2):
    loc1 = (26.862739, 75.778064)
    loc2 = (26.9149810000001, 75.7452000000001)
    print(hs.haversine(loc1, loc2))


def getGeoLocation(address):
    locationData = dict()
    try:
        # REST API path for getting location details
        response = requests.get('https://geocode.search.hereapi.com/v1/geocode?q=' + address + '&apiKey=jAmlaOMhT1SlJH1PFB4xldFcWAS3pn7n0XFq39lBOlQ')
        locationData = json.loads(response.content.decode('utf-8'))
    except Exception as e:
        print(e)
        pass
    return locationData


@app.route('/', methods=["GET", "POST"])
def getLocationData():
    if request.method == "POST":
        address = request.values['address']
        if address:
            return getGeoLocation(address)
    return False
