import pandas as pd
import ast
import haversine as hs


def getDistanceBetweenTwoLocation(loc1, loc2):
    loc1 = (loc1['lat'], loc1['lng'])
    loc2 = (loc2['lat'], loc2['lng'])
    return hs.haversine(loc1, loc2)


sellerDF = pd.read_csv('sellerAdd.csv')
customerDF = pd.read_csv('customerAdd.csv')
seller_data = sellerDF.values.tolist()
customer_data = customerDF.values.tolist()

for customerAdd in customer_data:
    print(customerAdd)
    customer_add = dict()
    if '{' in str(customerAdd[11]):
        customer_add = dict(ast.literal_eval(customerAdd[11]))
    else:
        customer_add = dict(ast.literal_eval(customerAdd[10]))
        # print("customer_add", customer_add)
    distance = []
    seller_Ids = []
    for sellerAdd in seller_data:
        seller_add = dict(ast.literal_eval(sellerAdd[8]))
        seller_Id = sellerAdd[0]
        seller_Ids.append(seller_Id)
        if 'items' in list(customer_add.keys()) and 'items' in list(seller_add.keys()):
            new_customer_add = customer_add["items"]
            new_seller_add = seller_add["items"]
            if len(new_customer_add) > 0 and len(new_seller_add) > 0:
                # print("customer_add=", str(customer_add[0]['position']))
                # print("seller_add=", str(seller_add[0]['position']))
                distance.append([getDistanceBetweenTwoLocation(new_customer_add[0]['position'],
                                                               new_seller_add[0]['position']), seller_Id])
    if len(distance) > 0:
        distance = sorted(distance, key=lambda x: x[0])
        closestSeller = seller_data[seller_Ids.index(distance[0][1])][2:8]
        closestSeller = [str(x) for x in closestSeller]
        customerAdd.append([distance[0][0], " ".join(closestSeller).replace("nan", "")])
        # print([distance[0][0], " ".join(closestSeller).replace("nan", "")])

finalDf = pd.DataFrame(customer_data)
finalDf.to_csv("customerMinDistanceFull.csv")
print("Completed")
